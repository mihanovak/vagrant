#!/usr/bin/env bash

# 0. Install the EPEL repository (for pip)
rpm -iUvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm

# 1. Install python development headers and libraries, python pip and git
sudo yum -y install python-devel python-pip numpy git

# 2. Install ipython
pip install "ipython[notebook]"

# 3. Check for Java installation
if type -p java >> /dev/null; then
   _java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
   _java="$JAVA_HOME/bin/java"
else
   # 3.1. Download and install Oracle JDK -> Java version >= 1.7
   curl -LO "http://download.oracle.com/otn-pub/java/jdk/8u31-b13/jdk-8u31-linux-x64.rpm" -H 'Cookie: oraclelicense=accept-securebackup-cookie' && rpm -i jdk-8u31-linux-x64.rpm; rm -f jdk-8u31-linux-x64.rpm; yum clean all
fi

# 4. Set JAVA_HOME environment variable
JAVA_HOME="export JAVA_HOME=/usr/java/default"
if ! grep -Fxq "$JAVA_HOME" /home/vagrant/.bashrc; then
   echo "export JAVA_HOME=/usr/java/default" >> /home/vagrant/.bashrc
fi

# 5. Check if openxal directory exist, if not create it and clone openxal-installation directory from bitbucket
if [ ! -d "/home/vagrant/openxal" ]; then
  mkdir -p /home/vagrant/openxal
  cd /home/vagrant/openxal
  git clone https://bitbucket.org/europeanspallationsource/openxal-installation
  
fi

source /home/vagrant/openxal/openxal-installation/openxal-environment.sh

# 6. Install / update openxal, jpype
/home/vagrant/openxal/openxal-installation/install-update.sh	
/home/vagrant/openxal/openxal-installation/install-update-jpype.sh

# 7. Set environment variables
OPENXAL_ENVIRONMENT="source /home/vagrant/openxal/openxal-installation/openxal-environment.sh"
if ! grep -Fxq "$OPENXAL_ENVIRONMENT" /home/vagrant/.bashrc; then
	echo "source /home/vagrant/openxal/openxal-installation/openxal-environment.sh" >> /etc/profile.d/openxal-environment.sh
	chmod +x /etc/profile.d/openxal-environment.sh
fi

chown -R vagrant /home/vagrant/openxal

# 8. Start ipython in background
iptables -F
ipython notebook --ipython-dir="/home/vagrant/data" --ip="*" --port="7878" &